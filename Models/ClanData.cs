﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClashOfTitans.Models
{

    public class ClanData
    {
        public List<Item> items { get; set; }
    }

    public class Item
    {
        public string name { get; set; }
        public string role { get; set; }
        public int expLevel { get; set; }
        public League league { get; set; }
        public int trophies { get; set; }
        public int clanRank { get; set; }
        public int previousClanRank { get; set; }
        public int donations { get; set; }
        public int donationsReceived { get; set; }
    }

    public class League
    {
        public int id { get; set; }
        public string name { get; set; }
        public Iconurls iconUrls { get; set; }
    }

    public class Iconurls
    {
        public string small { get; set; }
        public string tiny { get; set; }
        public string medium { get; set; }
    }

}