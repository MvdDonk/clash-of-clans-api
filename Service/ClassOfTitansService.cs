﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using ClashOfTitans.Models;
using RestSharp;

namespace ClashOfTitans.Service
{
    class ClassOfTitansService
    {
        private RestClient client;

        private static ClassOfTitansService _Instance;
        public static ClassOfTitansService Instance
        {
            get { return _Instance ?? (_Instance = new ClassOfTitansService()); }
        }

        private ClassOfTitansService()
        {
            var apiKey = WebConfigurationManager.AppSettings["ClashOfTitansApiKey"];
            client = new RestClient(WebConfigurationManager.AppSettings["ClashOfTitansApiUrl"]);
            client.AddDefaultHeader("authorization", string.Format("Bearer {0}", apiKey));
        }

        public ClanData GetClanMembers(string clanId)
        {
            var request = new RestRequest(string.Format("/clans/%23{0}/members", clanId), Method.GET);
            var response = client.Execute<ClanData>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            else
            {
                return null;
            }
        }
    }
}
