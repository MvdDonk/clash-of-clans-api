﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClashOfTitans.Service;

namespace ClashOfTitans.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var clans = ClassOfTitansService.Instance.GetClanMembers("9C2GYJ9V");
            return View(clans);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}