﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClashOfTitans.Startup))]
namespace ClashOfTitans
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
